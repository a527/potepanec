Spree::Product.class_eval do
  scope :includes_price_and_images, -> {
    includes(master: [:default_price, :images])
  }

  scope :sort_products_by, -> (sort) {
    case sort
    when 'LOW_PRICE'
      except(:order).ascend_by_master_price
    when 'HIGH_PRICE'
      except(:order).descend_by_master_price
    when 'OLD_PRODUCTS'
      except(:order).order(available_on: :asc)
    else
      except(:order).order(available_on: :desc)
    end
  }

  scope :search_by, -> (search_word) {
    where('name like :search OR description like :search', search: "%#{search_word}%")
  }

  scope :related_products, -> (product) {
    in_taxons(product.taxons).where.not(id: product.id).distinct
  }
end
