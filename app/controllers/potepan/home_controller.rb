class Potepan::HomeController < ApplicationController
  NEW_PRODUCTS_MAXIMUM = 8

  def index
    @new_products = Spree::Product.sort_products_by('NEW_PRODUCTS').limit(NEW_PRODUCTS_MAXIMUM).includes_price_and_images
  end
end
