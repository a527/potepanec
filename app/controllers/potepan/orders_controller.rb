class Potepan::OrdersController < ApplicationController
  # current_order
  include Spree::Core::ControllerHelpers::Order
  # current_store
  include Spree::Core::ControllerHelpers::Store
  # try_spree_current_user
  include Spree::Core::ControllerHelpers::Auth
  # permitted_order_attributes
  include Spree::Core::ControllerHelpers::StrongParameters

  def edit
    @order = current_order || Spree::Order.incomplete.find_or_initialize_by(guest_token: cookies.signed[:guest_token])
    @line_items = @order.line_items.includes(variant: [:product])
  end

  def create
    @order = current_order(create_order_if_necessary: true)
    authorize! :update, @order, cookies.signed[:guest_token]

    variant  = Spree::Variant.find(params[:variant_id])
    quantity = params[:quantity].present? ? params[:quantity].to_i : 1

    # 2,147,483,647 is crazy. See issue https://github.com/spree/spree/issues/2695.
    if !quantity.between?(1, 2_147_483_647)
      @order.errors.add(:base, t('spree.please_enter_reasonable_quantity'))
    end

    begin
      @line_item = @order.contents.add(variant, quantity)
    rescue ActiveRecord::RecordInvalid => error
      @order.errors.add(:base, error.record.errors.full_messages.join(", "))
    end

    redirect_to potepan_cart_path
  end

  def update
    @order = current_order
    if @order.contents.update_cart(order_params)
      if params.key?(:checkout) && @order.cart?
        @order.next
      elsif params.key?(:checkout)
        redirect_to potepan_checkout_state_path(@order.checkout_steps.first) && return
      end
    end
    redirect_to potepan_cart_path
  end

  private

  def order_params
    if params[:order]
      params[:order].permit(*permitted_order_attributes)
    end
  end
end
