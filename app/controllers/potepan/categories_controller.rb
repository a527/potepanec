class Potepan::CategoriesController < ApplicationController
  # GET /potepan/categories/:id(.:format)
  def show
    @taxon = Spree::Taxon.find(params[:id])
    @taxonomies = Spree::Taxonomy.all
    @products = Spree::Product.in_taxon(@taxon).includes_price_and_images.sort_products_by(params[:sort])
    @colors = Spree::OptionType.find_by(presentation: "Color").option_values
    @sizes = Spree::OptionType.find_by(presentation: "Size").option_values

    if params[:color]
      @products = Spree::Product.in_taxon(@taxon).with(params[:color])
    end
    if params[:size]
      @products = Spree::Product.in_taxon(@taxon).with(params[:size])
    end
  end
end
