class Potepan::ProductsController < ApplicationController
  RELATED_PRODUCTS_MAXIMUM = 4

  # GET /potepan/products/:id(.:format)
  def show
    @product = Spree::Product.find(params[:id])
    @related_products = Spree::Product.related_products(@product).limit(RELATED_PRODUCTS_MAXIMUM).includes_price_and_images
  end

  # GET /potepan/products/search(.:format)
  def search
    @products = Spree::Product.search_by(params[:search]).includes_price_and_images
  end
end
