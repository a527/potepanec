class Potepan::LineItemsController < ApplicationController
  # current_order
  include Spree::Core::ControllerHelpers::Order
  # current_store
  include Spree::Core::ControllerHelpers::Store
  # try_spree_current_user
  include Spree::Core::ControllerHelpers::Auth

  def destroy
    current_order.contents.remove_line_item(Spree::LineItem.find(params[:id]))
    redirect_to potepan_cart_path
  end
end
