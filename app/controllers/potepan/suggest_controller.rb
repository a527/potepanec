class Potepan::SuggestController < ApplicationController
  def index
    @result = auto_complete(params[:keyword])
    respond_to do |format|
      format.json { render json: @result }
    end
  end
end
