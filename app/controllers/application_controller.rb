class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  def auto_complete(keyword)
    require 'net/http'
    if keyword.present?
      params = URI.encode_www_form([["keyword", keyword], ["max_num", 5]])
      uri = URI.parse(Rails.application.credentials.api[:API_URL] + "?" + params)
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      request = Net::HTTP::Get.new(uri.request_uri)
      request['Authorization'] = 'Bearer ' + Rails.application.credentials.api[:API_KEY]
      response = http.request(request)
      if response.code == "200"
        JSON.parse(response.body)
      else
        []
      end
    end
  end
end
