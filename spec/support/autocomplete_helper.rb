module AutocompleteHelpers
  def autocomplete
    WebMock.stub_request(:get, Rails.application.credentials.api[:API_URL]).
      with(
        headers: {
          'Content-Type' => 'application/json',
        }
      ).
      to_return(
        status: 200,
        body: ""
      )
  end
end
