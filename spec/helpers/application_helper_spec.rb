require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helper do
  let(:product) { create(:product) }

  describe "full_titleヘルパー" do
    context "page_titleなし" do
      it "base_titleのみ" do
        expect(full_title('')).to eq "BIGBAG Store"
      end

      it "base_titleのみ(引数nil)" do
        expect(full_title(nil)).to eq "BIGBAG Store"
      end
    end

    context "page_titleあり" do
      it "page_title(product.name) - base_title" do
        expect(full_title(product.name)).to eq "#{product.name} - BIGBAG Store"
      end
    end
  end
end
