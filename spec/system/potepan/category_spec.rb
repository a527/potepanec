require 'rails_helper'

RSpec.describe "カテゴリページ", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { taxonomy.root.children.create(name: "Categories") }
  let(:taxon1) { create(:taxon, name: "taxon1", taxonomy: taxonomy, parent_id: taxon.id) }
  let(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent_id: taxon.id) }
  let(:variant) { create(:variant, option_values: [red, xlarge]) }
  let(:color) { create(:option_type, presentation: 'Color') }
  let(:size) { create(:option_type, presentation: 'Size') }
  let(:red) { create(:option_value, name: 'Red', option_type: color, presentation: 'Red') }
  let(:xlarge) { create(:option_value, name: 'XLarge', option_type: size, presentation: 'XL') }
  let!(:product1) do
    create(:product, name: "product1", price: "18.99", taxons: [taxon1], option_types: [color, size],
                     variants: [variant], available_on: 3.years.ago)
  end
  let!(:product2) { create(:product, name: "product2", price: "19.99", taxons: [taxon1], available_on: Time.now) }
  let!(:product3) { create(:product, name: "product3", price: "20.99", taxons: [taxon1], available_on: 1.years.ago) }
  let!(:product4) { create(:product, name: "product4", price: "21.99", taxons: [taxon2]) }

  describe 'カテゴリページ' do
    before { visit potepan_category_path(taxon1.id) }

    context '商品カテゴリー' do
      example 'カテゴリ一覧が表示されていること' do
        expect(page).to have_content "Categories"
        expect(page).to have_content "taxon1"
        expect(page).to have_content "taxon2"
      end

      example "該当カテゴリ商品の情報が全て表示されていること" do
        expect(page).to have_content product1.name
        expect(page).to have_content product1.price
        expect(page).to have_content product2.name
        expect(page).to have_content product2.price
        expect(page).to have_content product3.name
        expect(page).to have_content product3.price
      end

      example "該当カテゴリ商品以外の情報が表示されていないこと" do
        expect(page).not_to have_content product4.name
        expect(page).not_to have_content product4.price
      end

      example "カテゴリ遷移後、該当カテゴリの情報が表示されていること" do
        click_on "Categories"
        click_on 'taxon2'
        expect(current_path).to eq potepan_category_path(taxon2.id)
        expect(page).to have_content product4.name
        expect(page).to have_content product4.price
      end

      example "カテゴリ遷移後、該当カテゴリ以外の情報が表示されていないこと" do
        click_on "Categories"
        click_on 'taxon2'
        expect(page).not_to have_content product1.name
        expect(page).not_to have_content product1.price
        expect(page).not_to have_content product2.name
        expect(page).not_to have_content product2.price
        expect(page).not_to have_content product3.name
        expect(page).not_to have_content product3.price
      end
    end

    context '色から探す' do
      example "色一覧が表示されていること" do
        expect(page).to have_content red.presentation
      end

      example "該当の商品が表示されていること" do
        click_on 'Red'
        expect(page).to have_content product1.name
        expect(page).to have_content product1.price
      end

      example "該当の商品以外が表示されていないこと" do
        click_on 'Red'
        expect(page).not_to have_content product2.name
        expect(page).not_to have_content product2.price
        expect(page).not_to have_content product3.name
        expect(page).not_to have_content product3.price
        expect(page).not_to have_content product4.name
        expect(page).not_to have_content product4.price
      end
    end

    context 'サイズから探す' do
      example "サイズ一覧が表示されていること" do
        expect(page).to have_content xlarge.presentation
      end

      example "該当の商品が表示されていること" do
        click_on 'XL'
        expect(page).to have_content product1.name
        expect(page).to have_content product1.price
      end

      example "該当の商品以外が表示されていないこと" do
        click_on 'XL'
        expect(page).not_to have_content product2.name
        expect(page).not_to have_content product2.price
        expect(page).not_to have_content product3.name
        expect(page).not_to have_content product3.price
        expect(page).not_to have_content product4.name
        expect(page).not_to have_content product4.price
      end
    end

    context '並び替え' do
      example "新着順に並べられていること（デフォルト）" do
        within(".col-md-9") do
          within(".row:last-child") do
            within(".col-sm-4:nth-child(1)") do
              expect(page).to have_content product2.name
            end
            within(".col-sm-4:nth-child(2)") do
              expect(page).to have_content product3.name
            end

            within(".col-sm-4:nth-child(3)") do
              expect(page).to have_content product1.name
            end
          end
        end
      end

      example "安い順に並べられていること" do
        visit potepan_category_path(taxon1.id, params: { sort: 'LOW_PRICE' })

        expect(page).to have_select('sort', selected: '安い順')

        within(".col-md-9") do
          within(".row:last-child") do
            within(".col-sm-4:nth-child(1)") do
              expect(page).to have_content product1.name
            end
            within(".col-sm-4:nth-child(2)") do
              expect(page).to have_content product2.name
            end

            within(".col-sm-4:nth-child(3)") do
              expect(page).to have_content product3.name
            end
          end
        end
      end

      example "高い順に並べられていること", js: true do
        visit potepan_category_path(taxon1.id, params: { sort: 'HIGH_PRICE' })

        expect(page).to have_select('sort', selected: '高い順')

        within(".col-md-9") do
          within(".row:last-child") do
            within(".col-sm-4:nth-child(1)") do
              expect(page).to have_content product3.name
            end
            within(".col-sm-4:nth-child(2)") do
              expect(page).to have_content product2.name
            end

            within(".col-sm-4:nth-child(3)") do
              expect(page).to have_content product1.name
            end
          end
        end
      end

      example "古い順に並べられていること" do
        visit potepan_category_path(taxon1.id, params: { sort: 'OLD_PRODUCTS' })
        expect(page).to have_select('sort', selected: '古い順')

        within(".col-md-9") do
          within(".row:last-child") do
            within(".col-sm-4:nth-child(1)") do
              expect(page).to have_content product1.name
            end
            within(".col-sm-4:nth-child(2)") do
              expect(page).to have_content product3.name
            end

            within(".col-sm-4:nth-child(3)") do
              expect(page).to have_content product2.name
            end
          end
        end
      end
    end

    example "商品詳細ページへ遷移し、該当商品の情報が表示されていること" do
      click_on 'product1'
      expect(current_path).to eq potepan_product_path(product1.id)
      expect(page).to have_content product1.name
      expect(page).to have_content product1.price
      expect(page).to have_content product1.description
    end

    example "一覧ページに戻れること" do
      click_on 'product1'
      click_on '一覧ページへ戻る'
      expect(current_path).to eq potepan_category_path(taxon1.id)
    end

    example "Homeに戻れること" do
      within(".lightSection") do
        click_on 'Home'
        expect(current_path).to eq potepan_root_path
      end
    end
  end
end
