require 'rails_helper'

RSpec.describe "homeページ", type: :system do
  let(:taxon) { create(:taxon) }
  let!(:product1) { create(:product, name: "product1", available_on: Time.now, taxons: [taxon]) }
  let!(:product2) { create(:product, name: "product2", available_on: 10.minutes.ago) }
  let!(:new_products) { create_list(:product, 10, available_on: 20.minutes.ago) }

  before { visit potepan_root_path }

  describe '新着商品' do
    example '正しい新着商品が表示されていること' do
      expect(page).to have_content product1.name
      expect(page).to have_content product2.name
    end

    example '新着商品を表示するhtml要素が最大8つであること' do
      within(".featuredProductsSlider") do
        expect(page).to have_selector('.slide', count: 8)
      end
    end

    example '順序が適切であること' do
      within(".featuredProductsSlider") do
        within(".slide:nth-child(1)") do
          expect(page).to have_content product1.name
        end

        within(".slide:nth-child(2)") do
          expect(page).to have_content product2.name
        end
      end
    end

    example '商品ページに遷移できること' do
      within(".featuredProductsSlider") do
        click_on product1.name
        expect(current_path).to eq potepan_product_path(product1.id)
      end
    end
  end
end
