require 'rails_helper'

RSpec.describe "検索機能", type: :system do
  let!(:product1) { create(:product, name: "product1", price: "18.99", description: "this is the first one") }
  let!(:product2) { create(:product, name: "product2", price: "19.99", description: "this is the second one") }

  describe '商品名から検索' do
    before do
      visit search_potepan_products_path(search: 'product1')
    end

    example '商品名から検索し、正しい結果を得られること' do
      expect(page).to have_content product1.name
      expect(page).to have_content product1.price
    end

    example "正しい結果以外が表示されていないこと" do
      expect(page).not_to have_content product2.name
      expect(page).not_to have_content product2.price
    end
  end

  describe '説明文から検索' do
    before do
      visit search_potepan_products_path(search: 'first')
    end

    example '説明文から検索し、正しい結果を得られること' do
      expect(page).to have_content product1.name
      expect(page).to have_content product1.price
    end

    example "正しい結果以外が表示されていないこと" do
      expect(page).not_to have_content product2.name
      expect(page).not_to have_content product2.price
    end
  end
end
