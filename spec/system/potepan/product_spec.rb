require 'rails_helper'

RSpec.describe "商品ページ", type: :system do
  let(:taxonomy) { create(:taxonomy, name: "Categories") }
  let(:taxon) { taxonomy.root.children.create(name: "Categories") }
  let(:taxon1) { create(:taxon, name: "taxon1", taxonomy: taxonomy, parent_id: taxon.id) }
  let(:taxon2) { create(:taxon, name: "taxon2", taxonomy: taxonomy, parent_id: taxon.id) }
  let!(:product1) { create(:product, name: "product1", price: "18.99", taxons: [taxon1]) }
  let!(:product2) { create(:product, name: "product2", price: "19.99", taxons: [taxon2]) }
  let!(:related_product) { create_list(:product, 5, taxons: [taxon1]) }

  describe '関連商品の表示' do
    before { visit potepan_product_path(product1.id) }

    example '正しい関連商品の情報が表示されていること' do
      within(".productsContent") do
        4.times do |index|
          expect(page).to have_content related_product[index].name
          expect(page).to have_content related_product[index].price
        end
      end
    end

    example "正しい関連商品以外が表示されていないこと" do
      within(".productsContent") do
        expect(page).not_to have_content product2.name
      end
    end

    example "関連商品を表示するHTML要素が最大4つであること" do
      within(".productsContent") do
        expect(page).to have_selector('.productBox', count: 4)
      end
    end

    example "自分自身の商品が表示されていないこと" do
      within(".productsContent") do
        expect(page).not_to have_content product1.name
      end
    end

    example "関連商品から別の商品のページへ遷移できること" do
      within(".productsContent") do
        click_on related_product[0].name
        expect(current_path).to eq potepan_product_path(related_product[0].id)
      end
    end
  end
end
