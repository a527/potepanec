require 'rails_helper'

RSpec.describe Potepan::SuggestController, type: :controller do
  before do
    autocomplete
    get :index, format: :json
  end

  example "正常なレスポンスを得る" do
    expect(response.status).to eq 200
  end
end
