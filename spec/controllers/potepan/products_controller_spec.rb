require 'rails_helper'
require 'webmock/rspec'
WebMock.disable_net_connect!(allow_localhost: true)

RSpec.describe Potepan::ProductsController, type: :controller do
  describe "show: GET /potepan/products/:id(.:format)" do
    let(:taxonomy) { create(:taxonomy, name: "Categories") }
    let(:taxon) { taxonomy.root.children.create(name: "Categories") }
    let(:taxon1) { create(:taxon, name: "taxon", taxonomy: taxonomy, parent_id: taxon.id) }
    let!(:product1) { create(:product, name: "product1", price: "18.99", taxons: [taxon1]) }
    let!(:product2) { create(:product, name: "product2", price: "19.99", taxons: [taxon1]) }

    before do
      get :show, params: { id: product1.id }
    end

    example "正常なレスポンスを得る" do
      expect(response.status).to eq 200
    end

    example "テンプレートが表示されていること" do
      expect(response).to render_template("potepan/products/show")
    end

    example "インスタンス変数@productが取得されていること" do
      expect(assigns(:product)).to eq product1
    end

    example "インスタンス変数@related_productsが取得されていること" do
      expect(assigns(:related_products)).to contain_exactly(product2)
    end
  end

  describe "search: GET /potepan/products/search(.:format)" do
    let!(:product1) { create(:product, name: "product1") }
    let!(:product2) { create(:product, name: "product2") }

    before do
      get :search, params: { search: 'product1' }
    end

    example "正常なレスポンスを得る" do
      expect(response.status).to eq 200
    end

    example "テンプレートが表示されている" do
      expect(response).to render_template("potepan/products/search")
    end

    example "インスタンス変数@productsが取得されていること" do
      expect(assigns(:products)).to contain_exactly(product1)
    end
  end
end
