require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe "show: GET /potepan/categories/:id(.:format)" do
    let!(:product) { create(:product, name: 'product', taxons: [taxon], option_types: [color, size]) }
    let(:taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:taxon) { create(:taxon, name: 'taxon', taxonomy: taxonomy) }
    let(:color) { create(:option_type, name: 'Color', presentation: 'Color') }
    let(:size) { create(:option_type, name: 'Size', presentation: 'Size') }
    let(:red) { create(:option_value, name: 'Red', option_type: color) }
    let(:small) { create(:option_value, name: 'Small', option_type: size) }

    before do
      get :show, params: { id: taxon.id }
    end

    example "正常なレスポンスを得る" do
      expect(response.status).to eq 200
    end

    example "インスタンス変数@taxonが取得できている" do
      expect(assigns(:taxon)).to eq taxon
    end

    example "インスタンス変数@taxonomiesが取得できている" do
      expect(assigns(:taxonomies)).to contain_exactly(taxonomy)
    end

    example "インスタンス変数@productsが取得できている" do
      expect(assigns(:products)).to contain_exactly(product)
    end

    example "インスタンス変数@colorsが取得できている" do
      expect(assigns(:colors)).to contain_exactly(red)
    end

    example "インスタンス変数@sizesが取得できている" do
      expect(assigns(:sizes)).to contain_exactly(small)
    end

    example "showページが表示されている" do
      expect(response).to render_template :show
    end
  end
end
