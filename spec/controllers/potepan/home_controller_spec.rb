require 'rails_helper'

RSpec.describe Potepan::HomeController, type: :controller do
  describe "show: GET /potepan/products/:id(.:format)" do
    let!(:product1) { create(:product, name: "product1", available_on: Date.today) }
    let!(:product2) { create(:product, name: "product2", available_on: 1.month.ago) }

    before do
      get :index
    end

    example "正常なレスポンスを得ること" do
      expect(response.status).to eq 200
    end

    example "変数@new_productsが取得できていること" do
      expect(assigns(:new_products)).to contain_exactly(product1, product2)
    end

    example "テンプレートが表示されていること" do
      expect(response).to render_template("potepan/home/index")
    end
  end
end
